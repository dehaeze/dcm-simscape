<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<!-- 2022-06-02 Thu 18:11 -->
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ESRF Double Crystal Monochromator - Dynamical Multi-Body Model</title>
<meta name="author" content="Dehaeze Thomas" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="https://research.tdehaeze.xyz/css/style.css"/>
<script type="text/javascript" src="https://research.tdehaeze.xyz/js/script.js"></script>
<script>
           MathJax = {
             svg: {
               scale: 1,
               fontCache: "global"
             },
             tex: {
               tags: "ams",
               multlineWidth: "%MULTLINEWIDTH",
               tagSide: "right",
                       macros: {bm: ["\\boldsymbol{#1}",1],},
               tagIndent: ".8em"
             }
           };
           </script>
           <script id="MathJax-script" async
             src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js"></script>
</head>
<body>
<div id="org-div-home-and-up">
 <a accesskey="h" href="../index.html"> UP </a>
 |
 <a accesskey="H" href="../index.html"> HOME </a>
</div><div id="content" class="content">
<h1 class="title">ESRF Double Crystal Monochromator - Dynamical Multi-Body Model</h1>
<div id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#org7ab138f">1. Open Loop System Identification</a>
<ul>
<li><a href="#org4bdd8a8">1.1. Identification</a></li>
<li><a href="#org308c21c">1.2. Plant in the frame of the fastjacks</a></li>
<li><a href="#org8722f96">1.3. Plant in the frame of the crystal</a></li>
</ul>
</li>
<li><a href="#org7a5b9b3">2. Open-Loop Noise Budgeting</a>
<ul>
<li><a href="#org331710d">2.1. Power Spectral Density of signals</a></li>
<li><a href="#org3bca0d9">2.2. Open Loop disturbance and measurement noise</a></li>
</ul>
</li>
<li><a href="#org808e143">3. Active Damping Plant (Strain gauges)</a>
<ul>
<li><a href="#org0ab2a87">3.1. Identification</a></li>
<li><a href="#org3834863">3.2. Relative Active Damping</a></li>
<li><a href="#org972c8d8">3.3. Damped Plant</a></li>
</ul>
</li>
<li><a href="#orgf406d83">4. Active Damping Plant (Force Sensors)</a>
<ul>
<li><a href="#org9fe5c0e">4.1. Identification</a></li>
<li><a href="#org3fa24b8">4.2. Controller - Root Locus</a></li>
<li><a href="#orge5d3222">4.3. Damped Plant</a></li>
</ul>
</li>
<li><a href="#org495a09b">5. Feedback Control</a></li>
<li><a href="#orgefe0221">6. HAC-LAC (IFF) architecture</a>
<ul>
<li><a href="#org1c6149b">6.1. System Identification</a></li>
<li><a href="#orge6cba43">6.2. High Authority Controller</a></li>
<li><a href="#org3cc0706">6.3. Performances</a></li>
<li><a href="#org918f082">6.4. Close Loop noise budget</a></li>
</ul>
</li>
</ul>
</div>
</div>
<hr>
<p>This report is also available as a <a href="./dcm-simscape-model.pdf">pdf</a>.</p>
<hr>

<p>
In this document, a Simscape (.e.g. multi-body) model of the ESRF Double Crystal Monochromator (DCM) is presented and used to develop and optimize the control strategy.
</p>

<p>
It is structured as follow:
</p>
<ul class="org-ul">
<li>Section <a href="#org566f204">1</a>: the system dynamics is identified in the absence of control.</li>
<li>Section <a href="#orgabe857c">2</a>: an open-loop noise budget is performed.</li>
<li>Section <a href="#orgbcf21ac">3</a>: it is studied whether if the strain gauges fixed to the piezoelectric actuators can be used to actively damp the plant.</li>
<li>Section <a href="#orgecb6c39">4</a>: piezoelectric force sensors are added in series with the piezoelectric actuators and are used to actively damp the plant using the Integral Force Feedback (IFF) control strategy.</li>
<li>Section <a href="#org4ff1527">6</a>: the High Authority Control - Low Authority Control (HAC-LAC) strategy is tested on the Simscape model.</li>
</ul>

<div id="outline-container-org7ab138f" class="outline-2">
<h2 id="org7ab138f"><span class="section-number-2">1.</span> Open Loop System Identification</h2>
<div class="outline-text-2" id="text-1">
<p>
<a id="org566f204"></a>
</p>
</div>
<div id="outline-container-org4bdd8a8" class="outline-3">
<h3 id="org4bdd8a8"><span class="section-number-3">1.1.</span> Identification</h3>
<div class="outline-text-3" id="text-1-1">
<p>
Let&rsquo;s considered the system \(\bm{G}(s)\) with:
</p>
<ul class="org-ul">
<li>3 inputs: force applied to the 3 fast jacks</li>
<li>3 outputs: measured displacement by the 3 interferometers pointing at the ring second crystal</li>
</ul>

<p>
It is schematically shown in Figure <a href="#orgc25dd36">1</a>.
</p>


<div id="orgc25dd36" class="figure">
<p><img src="figs/schematic_system_inputs_outputs.png" alt="schematic_system_inputs_outputs.png" />
</p>
<p><span class="figure-number">Figure 1: </span>Dynamical system with inputs and outputs</p>
</div>

<p>
The system is identified from the Simscape model.
</p>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Input/Output definition</span>
clear io; io_i = 1;

<span class="org-matlab-cellbreak">%% Inputs</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Control Input {3x1} [N]</span>
io(io_i) = linio([mdl, <span class="org-string">'/control_system'</span>], 1, <span class="org-string">'openinput'</span>);  io_i = io_i <span class="org-builtin">+</span> 1;

<span class="org-matlab-cellbreak">%% Outputs</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Interferometers {3x1} [m]</span>
io(io_i) = linio([mdl, <span class="org-string">'/DCM'</span>], 1, <span class="org-string">'openoutput'</span>); io_i = io_i <span class="org-builtin">+</span> 1;
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Extraction of the dynamics</span>
G = linearize(mdl, io);
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab">size(G)
</pre>
</div>

<pre class="example">
size(G)
State-space model with 3 outputs, 3 inputs, and 24 states.
</pre>
</div>
</div>

<div id="outline-container-org308c21c" class="outline-3">
<h3 id="org308c21c"><span class="section-number-3">1.2.</span> Plant in the frame of the fastjacks</h3>
<div class="outline-text-3" id="text-1-2">
<div class="org-src-container">
<pre class="src src-matlab">load(<span class="org-string">'dcm_kinematics.mat'</span>);
</pre>
</div>

<p>
Using the forward and inverse kinematics, we can computed the dynamics from piezo forces to axial motion of the 3 fastjacks (see Figure <a href="#org3a46abb">2</a>).
</p>


<div id="org3a46abb" class="figure">
<p><img src="figs/schematic_jacobian_frame_fastjack.png" alt="schematic_jacobian_frame_fastjack.png" />
</p>
<p><span class="figure-number">Figure 2: </span>Use of Jacobian matrices to obtain the system in the frame of the fastjacks</p>
</div>


<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Compute the system in the frame of the fastjacks</span>
G_pz = J_a_h<span class="org-builtin">*</span>inv(J_2h_s)<span class="org-builtin">*</span>G;
</pre>
</div>

<p>
The DC gain of the new system shows that the system is well decoupled at low frequency.
</p>
<div class="org-src-container">
<pre class="src src-matlab">dcgain(G_pz)
</pre>
</div>

<table id="org16a1bc3" border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
<caption class="t-above"><span class="table-number">Table 1:</span> DC gain of the plant in the frame of the fast jacks \(\bm{G}_{\text{fj}}\)</caption>

<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<tbody>
<tr>
<td class="org-right">4.4407e-09</td>
<td class="org-right">2.7656e-12</td>
<td class="org-right">1.0132e-12</td>
</tr>

<tr>
<td class="org-right">2.7656e-12</td>
<td class="org-right">4.4407e-09</td>
<td class="org-right">1.0132e-12</td>
</tr>

<tr>
<td class="org-right">1.0109e-12</td>
<td class="org-right">1.0109e-12</td>
<td class="org-right">4.4424e-09</td>
</tr>
</tbody>
</table>

<p>
The bode plot of \(\bm{G}_{\text{fj}}(s)\) is shown in Figure <a href="#orgd19fba2">3</a>.
</p>

<div class="org-src-container">
<pre class="src src-matlab">G_pz = diag(1<span class="org-builtin">./</span>diag(dcgain(G_pz)))<span class="org-builtin">*</span>G_pz;
</pre>
</div>


<div id="orgd19fba2" class="figure">
<p><img src="figs/bode_plot_plant_fj.png" alt="bode_plot_plant_fj.png" />
</p>
<p><span class="figure-number">Figure 3: </span>Bode plot of the diagonal and off-diagonal elements of the plant in the frame of the fast jacks</p>
</div>

<div class="important" id="org70bf5d1">
<p>
Computing the system in the frame of the fastjack gives good decoupling at low frequency (until the first resonance of the system).
</p>

</div>
</div>
</div>

<div id="outline-container-org8722f96" class="outline-3">
<h3 id="org8722f96"><span class="section-number-3">1.3.</span> Plant in the frame of the crystal</h3>
<div class="outline-text-3" id="text-1-3">

<div id="org331855a" class="figure">
<p><img src="figs/schematic_jacobian_frame_crystal.png" alt="schematic_jacobian_frame_crystal.png" />
</p>
<p><span class="figure-number">Figure 4: </span>Use of Jacobian matrices to obtain the system in the frame of the crystal</p>
</div>

<div class="org-src-container">
<pre class="src src-matlab">G_mr = inv(J_s_r)<span class="org-builtin">*</span>G<span class="org-builtin">*</span>inv(J_a_r<span class="org-builtin">'</span>);
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab">dcgain(G_mr)
</pre>
</div>

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<tbody>
<tr>
<td class="org-right">1.9978e-09</td>
<td class="org-right">3.9657e-09</td>
<td class="org-right">7.7944e-09</td>
</tr>

<tr>
<td class="org-right">3.9656e-09</td>
<td class="org-right">8.4979e-08</td>
<td class="org-right">-1.5135e-17</td>
</tr>

<tr>
<td class="org-right">7.7944e-09</td>
<td class="org-right">-3.9252e-17</td>
<td class="org-right">1.834e-07</td>
</tr>
</tbody>
</table>

<p>
This results in a coupled system.
The main reason is that, as we map forces to the center of the ring crystal and not at the center of mass/stiffness of the moving part, vertical forces will induce rotation and torques will induce vertical motion.
</p>
</div>
</div>
</div>

<div id="outline-container-org7a5b9b3" class="outline-2">
<h2 id="org7a5b9b3"><span class="section-number-2">2.</span> Open-Loop Noise Budgeting</h2>
<div class="outline-text-2" id="text-2">
<p>
<a id="orgabe857c"></a>
</p>

<div id="orgef00456" class="figure">
<p><img src="figs/noise_budget_dcm_schematic_fast_jack_frame.png" alt="noise_budget_dcm_schematic_fast_jack_frame.png" />
</p>
<p><span class="figure-number">Figure 5: </span>Schematic representation of the control loop in the frame of one fast jack</p>
</div>
</div>

<div id="outline-container-org331710d" class="outline-3">
<h3 id="org331710d"><span class="section-number-3">2.1.</span> Power Spectral Density of signals</h3>
<div class="outline-text-3" id="text-2-1">
<p>
Interferometer noise:
</p>
<div class="org-src-container">
<pre class="src src-matlab">Wn = 6e<span class="org-builtin">-</span>11<span class="org-builtin">*</span>(1 <span class="org-builtin">+</span> s<span class="org-builtin">/</span>2<span class="org-builtin">/</span><span class="org-matlab-math">pi</span><span class="org-builtin">/</span>200)<span class="org-builtin">/</span>(1 <span class="org-builtin">+</span> s<span class="org-builtin">/</span>2<span class="org-builtin">/</span><span class="org-matlab-math">pi</span><span class="org-builtin">/</span>60); <span class="org-comment-delimiter">% </span><span class="org-comment">m/sqrt(Hz)</span>
</pre>
</div>

<pre class="example">
Measurement noise: 0.79 [nm,rms]
</pre>


<p>
DAC noise (amplified by the PI voltage amplifier, and converted to newtons):
</p>
<div class="org-src-container">
<pre class="src src-matlab">Wdac = tf(3e<span class="org-builtin">-</span>8); <span class="org-comment-delimiter">% </span><span class="org-comment">V/sqrt(Hz)</span>
Wu = Wdac<span class="org-builtin">*</span>22.5<span class="org-builtin">*</span>10; <span class="org-comment-delimiter">% </span><span class="org-comment">N/sqrt(Hz)</span>
</pre>
</div>

<pre class="example">
DAC noise: 0.95 [uV,rms]
</pre>


<p>
Disturbances:
</p>
<div class="org-src-container">
<pre class="src src-matlab">Wd = 5e<span class="org-builtin">-</span>7<span class="org-builtin">/</span>(1 <span class="org-builtin">+</span> s<span class="org-builtin">/</span>2<span class="org-builtin">/</span><span class="org-matlab-math">pi</span>); <span class="org-comment-delimiter">% </span><span class="org-comment">m/sqrt(Hz)</span>
</pre>
</div>

<pre class="example">
Disturbance motion: 0.61 [um,rms]
</pre>


<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Save ASD of noise and disturbances</span>
save(<span class="org-string">'mat/asd_noises_disturbances.mat'</span>, <span class="org-string">'Wn'</span>, <span class="org-string">'Wu'</span>, <span class="org-string">'Wd'</span>);
</pre>
</div>
</div>
</div>

<div id="outline-container-org3bca0d9" class="outline-3">
<h3 id="org3bca0d9"><span class="section-number-3">2.2.</span> Open Loop disturbance and measurement noise</h3>
<div class="outline-text-3" id="text-2-2">
<p>
The comparison of the amplitude spectral density of the measurement noise and of the jack parasitic motion is performed in Figure <a href="#orgf0b0f03">6</a>.
It confirms that the sensor noise is low enough to measure the motion errors of the crystal.
</p>


<div id="orgf0b0f03" class="figure">
<p><img src="figs/open_loop_noise_budget_fast_jack.png" alt="open_loop_noise_budget_fast_jack.png" />
</p>
<p><span class="figure-number">Figure 6: </span>Open Loop noise budgeting</p>
</div>
</div>
</div>
</div>

<div id="outline-container-org808e143" class="outline-2">
<h2 id="org808e143"><span class="section-number-2">3.</span> Active Damping Plant (Strain gauges)</h2>
<div class="outline-text-2" id="text-3">
<p>
<a id="orgbcf21ac"></a>
</p>
<p>
In this section, we wish to see whether if strain gauges fixed to the piezoelectric actuator can be used for active damping.
</p>
</div>
<div id="outline-container-org0ab2a87" class="outline-3">
<h3 id="org0ab2a87"><span class="section-number-3">3.1.</span> Identification</h3>
<div class="outline-text-3" id="text-3-1">
<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Input/Output definition</span>
clear io; io_i = 1;

<span class="org-matlab-cellbreak">%% Inputs</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Control Input {3x1} [N]</span>
io(io_i) = linio([mdl, <span class="org-string">'/control_system'</span>], 1, <span class="org-string">'openinput'</span>);  io_i = io_i <span class="org-builtin">+</span> 1;

<span class="org-matlab-cellbreak">%% Outputs</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Strain Gauges {3x1} [m]</span>
io(io_i) = linio([mdl, <span class="org-string">'/DCM'</span>], 2, <span class="org-string">'openoutput'</span>); io_i = io_i <span class="org-builtin">+</span> 1;
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Extraction of the dynamics</span>
G_sg = linearize(mdl, io);
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab">dcgain(G_sg)
</pre>
</div>

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />
</colgroup>
<tbody>
<tr>
<td class="org-right">4.4443e-09</td>
<td class="org-right">1.0339e-13</td>
<td class="org-right">3.774e-14</td>
</tr>

<tr>
<td class="org-right">1.0339e-13</td>
<td class="org-right">4.4443e-09</td>
<td class="org-right">3.774e-14</td>
</tr>

<tr>
<td class="org-right">3.7792e-14</td>
<td class="org-right">3.7792e-14</td>
<td class="org-right">4.4444e-09</td>
</tr>
</tbody>
</table>


<div id="orgfdda5de" class="figure">
<p><img src="figs/strain_gauge_plant_bode_plot.png" alt="strain_gauge_plant_bode_plot.png" />
</p>
<p><span class="figure-number">Figure 7: </span>Bode Plot of the transfer functions from piezoelectric forces to strain gauges measuremed displacements</p>
</div>

<div class="important" id="org8c0fd92">
<p>
As the distance between the poles and zeros in Figure <a href="#orgd1f744c">10</a> is very small, little damping can be actively added using the strain gauges.
This will be confirmed using a Root Locus plot.
</p>

</div>
</div>
</div>

<div id="outline-container-org3834863" class="outline-3">
<h3 id="org3834863"><span class="section-number-3">3.2.</span> Relative Active Damping</h3>
<div class="outline-text-3" id="text-3-2">
<div class="org-src-container">
<pre class="src src-matlab">Krad_g1 = eye(3)<span class="org-builtin">*</span>s<span class="org-builtin">/</span>(s<span class="org-builtin">^</span>2<span class="org-builtin">/</span>(2<span class="org-builtin">*</span><span class="org-matlab-math">pi</span><span class="org-builtin">*</span>500)<span class="org-builtin">^</span>2 <span class="org-builtin">+</span> 2<span class="org-builtin">*</span>s<span class="org-builtin">/</span>(2<span class="org-builtin">*</span><span class="org-matlab-math">pi</span><span class="org-builtin">*</span>500) <span class="org-builtin">+</span> 1);
</pre>
</div>

<p>
As can be seen in Figure <a href="#orgd1eb86d">8</a>, very little damping can be added using relative damping strategy using strain gauges.
</p>


<div id="orgd1eb86d" class="figure">
<p><img src="figs/relative_damping_root_locus.png" alt="relative_damping_root_locus.png" />
</p>
<p><span class="figure-number">Figure 8: </span>Root Locus for the relative damping control</p>
</div>

<div class="org-src-container">
<pre class="src src-matlab">Krad = <span class="org-builtin">-</span>g<span class="org-builtin">*</span>Krad_g1;
</pre>
</div>
</div>
</div>

<div id="outline-container-org972c8d8" class="outline-3">
<h3 id="org972c8d8"><span class="section-number-3">3.3.</span> Damped Plant</h3>
<div class="outline-text-3" id="text-3-3">
<p>
The controller is implemented on Simscape, and the damped plant is identified.
</p>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Input/Output definition</span>
clear io; io_i = 1;

<span class="org-matlab-cellbreak">%% Inputs</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Control Input {3x1} [N]</span>
io(io_i) = linio([mdl, <span class="org-string">'/control_system'</span>], 1, <span class="org-string">'input'</span>);  io_i = io_i <span class="org-builtin">+</span> 1;

<span class="org-matlab-cellbreak">%% Outputs</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Force Sensor {3x1} [m]</span>
io(io_i) = linio([mdl, <span class="org-string">'/DCM'</span>], 1, <span class="org-string">'openoutput'</span>); io_i = io_i <span class="org-builtin">+</span> 1;
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% DCM Kinematics</span>
load(<span class="org-string">'dcm_kinematics.mat'</span>);
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Identification of the Open Loop plant</span>
controller.type = 0; <span class="org-comment-delimiter">% </span><span class="org-comment">Open Loop</span>
G_ol = J_a_r<span class="org-builtin">*</span>inv(J_s_r)<span class="org-builtin">*</span>linearize(mdl, io);
G_ol.InputName  = {<span class="org-string">'u_ur'</span>,  <span class="org-string">'u_uh'</span>,  <span class="org-string">'u_d'</span>};
G_ol.OutputName = {<span class="org-string">'d_ur'</span>,  <span class="org-string">'d_uh'</span>,  <span class="org-string">'d_d'</span>};
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Identification of the damped plant with Relative Active Damping</span>
controller.type = 2; <span class="org-comment-delimiter">% </span><span class="org-comment">RAD</span>
G_dp = J_a_r<span class="org-builtin">*</span>inv(J_s_r)<span class="org-builtin">*</span>linearize(mdl, io);
G_dp.InputName  = {<span class="org-string">'u_ur'</span>,  <span class="org-string">'u_uh'</span>,  <span class="org-string">'u_d'</span>};
G_dp.OutputName = {<span class="org-string">'d_ur'</span>,  <span class="org-string">'d_uh'</span>,  <span class="org-string">'d_d'</span>};
</pre>
</div>


<div id="orgdfa8a77" class="figure">
<p><img src="figs/comp_damp_undamped_plant_rad_bode_plot.png" alt="comp_damp_undamped_plant_rad_bode_plot.png" />
</p>
<p><span class="figure-number">Figure 9: </span>Bode plot of both the open-loop plant and the damped plant using relative active damping</p>
</div>
</div>
</div>
</div>

<div id="outline-container-orgf406d83" class="outline-2">
<h2 id="orgf406d83"><span class="section-number-2">4.</span> Active Damping Plant (Force Sensors)</h2>
<div class="outline-text-2" id="text-4">
<p>
<a id="orgecb6c39"></a>
</p>
<p>
Force sensors are added above the piezoelectric actuators.
They can consists of a simple piezoelectric ceramic stack.
See for instance (<a href="#citeproc_bib_item_1">Fleming and Leang 2010</a>).
</p>
</div>
<div id="outline-container-org9fe5c0e" class="outline-3">
<h3 id="org9fe5c0e"><span class="section-number-3">4.1.</span> Identification</h3>
<div class="outline-text-3" id="text-4-1">
<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Input/Output definition</span>
clear io; io_i = 1;

<span class="org-matlab-cellbreak">%% Inputs</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Control Input {3x1} [N]</span>
io(io_i) = linio([mdl, <span class="org-string">'/control_system'</span>], 1, <span class="org-string">'openinput'</span>);  io_i = io_i <span class="org-builtin">+</span> 1;

<span class="org-matlab-cellbreak">%% Outputs</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Force Sensor {3x1} [m]</span>
io(io_i) = linio([mdl, <span class="org-string">'/DCM'</span>], 3, <span class="org-string">'openoutput'</span>); io_i = io_i <span class="org-builtin">+</span> 1;
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Extraction of the dynamics</span>
G_fs = linearize(mdl, io);
</pre>
</div>

<p>
The Bode plot of the identified dynamics is shown in Figure <a href="#orgd1f744c">10</a>.
At high frequency, the diagonal terms are constants while the off-diagonal terms have some roll-off.
</p>


<div id="orgd1f744c" class="figure">
<p><img src="figs/iff_plant_bode_plot.png" alt="iff_plant_bode_plot.png" />
</p>
<p><span class="figure-number">Figure 10: </span>Bode plot of IFF Plant</p>
</div>
</div>
</div>

<div id="outline-container-org3fa24b8" class="outline-3">
<h3 id="org3fa24b8"><span class="section-number-3">4.2.</span> Controller - Root Locus</h3>
<div class="outline-text-3" id="text-4-2">
<p>
We want to have integral action around the resonances of the system, but we do not want to integrate at low frequency.
Therefore, we can use a low pass filter.
</p>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Integral Force Feedback Controller</span>
Kiff_g1 = eye(3)<span class="org-builtin">*</span>1<span class="org-builtin">/</span>(1 <span class="org-builtin">+</span> s<span class="org-builtin">/</span>2<span class="org-builtin">/</span><span class="org-matlab-math">pi</span><span class="org-builtin">/</span>20);
</pre>
</div>


<div id="org7f81ce5" class="figure">
<p><img src="figs/iff_root_locus.png" alt="iff_root_locus.png" />
</p>
<p><span class="figure-number">Figure 11: </span>Root Locus plot for the IFF Control strategy</p>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Integral Force Feedback Controller with optimal gain</span>
Kiff = g<span class="org-builtin">*</span>Kiff_g1;
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Save the IFF controller</span>
save(<span class="org-string">'mat/Kiff.mat'</span>, <span class="org-string">'Kiff'</span>);
</pre>
</div>
</div>
</div>

<div id="outline-container-orge5d3222" class="outline-3">
<h3 id="orge5d3222"><span class="section-number-3">4.3.</span> Damped Plant</h3>
<div class="outline-text-3" id="text-4-3">
<p>
Both the Open Loop dynamics (see Figure <a href="#org3a46abb">2</a>) and the dynamics with IFF (see Figure <a href="#org4aafce8">12</a>) are identified.
</p>

<p>
We are here interested in the dynamics from \(\bm{u}^\prime = [u_{u_r}^\prime,\ u_{u_h}^\prime,\ u_d^\prime]\) (input of the damped plant) to \(\bm{d}_{\text{fj}} = [d_{u_r},\ d_{u_h},\ d_d]\) (motion of the crystal expressed in the frame of the fast-jacks).
This is schematically represented in Figure <a href="#org4aafce8">12</a>.
</p>


<div id="org4aafce8" class="figure">
<p><img src="figs/schematic_jacobian_frame_fastjack_iff.png" alt="schematic_jacobian_frame_fastjack_iff.png" />
</p>
<p><span class="figure-number">Figure 12: </span>Use of Jacobian matrices to obtain the system in the frame of the fastjacks</p>
</div>

<p>
The dynamics from \(\bm{u}\) to \(\bm{d}_{\text{fj}}\) (open-loop dynamics) and from \(\bm{u}^\prime\) to \(\bm{d}_{\text{fs}}\) are compared in Figure <a href="#orgbd8afb9">13</a>.
It is clear that the Integral Force Feedback control strategy is very effective in damping the resonances of the plant.
</p>


<div id="orgbd8afb9" class="figure">
<p><img src="figs/comp_damped_undamped_plant_iff_bode_plot.png" alt="comp_damped_undamped_plant_iff_bode_plot.png" />
</p>
<p><span class="figure-number">Figure 13: </span>Bode plot of both the open-loop plant and the damped plant using IFF</p>
</div>

<div class="important" id="org53702ee">
<p>
The Integral Force Feedback control strategy is very effective in damping the modes present in the plant.
</p>

</div>
</div>
</div>
</div>

<div id="outline-container-org495a09b" class="outline-2">
<h2 id="org495a09b"><span class="section-number-2">5.</span> Feedback Control</h2>
<div class="outline-text-2" id="text-5">

<div id="org14950fe" class="figure">
<p><img src="figs/schematic_jacobian_frame_fastjack_feedback.png" alt="schematic_jacobian_frame_fastjack_feedback.png" />
</p>
</div>
</div>
</div>

<div id="outline-container-orgefe0221" class="outline-2">
<h2 id="orgefe0221"><span class="section-number-2">6.</span> HAC-LAC (IFF) architecture</h2>
<div class="outline-text-2" id="text-6">
<p>
<a id="org4ff1527"></a>
</p>
<p>
The HAC-LAC architecture is shown in Figure <a href="#org8909d7d">14</a>.
</p>


<div id="org8909d7d" class="figure">
<p><img src="figs/schematic_jacobian_frame_fastjack_hac_iff.png" alt="schematic_jacobian_frame_fastjack_hac_iff.png" />
</p>
<p><span class="figure-number">Figure 14: </span>HAC-LAC architecture</p>
</div>
</div>
<div id="outline-container-org1c6149b" class="outline-3">
<h3 id="org1c6149b"><span class="section-number-3">6.1.</span> System Identification</h3>
<div class="outline-text-3" id="text-6-1">
<p>
Let&rsquo;s identify the damped plant.
</p>


<div id="org1ad0ce7" class="figure">
<p><img src="figs/bode_plot_hac_iff_plant.png" alt="bode_plot_hac_iff_plant.png" />
</p>
<p><span class="figure-number">Figure 15: </span>Bode Plot of the plant for the High Authority Controller (transfer function from \(\bm{u}^\prime\) to \(\bm{\epsilon}_d\))</p>
</div>
</div>
</div>

<div id="outline-container-orge6cba43" class="outline-3">
<h3 id="orge6cba43"><span class="section-number-3">6.2.</span> High Authority Controller</h3>
<div class="outline-text-3" id="text-6-2">
<p>
Let&rsquo;s design a controller with a bandwidth of 100Hz.
As the plant is well decoupled and well approximated by a constant at low frequency, the high authority controller can easily be designed with SISO loop shaping.
</p>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Controller design</span>
wc = 2<span class="org-builtin">*</span><span class="org-matlab-math">pi</span><span class="org-builtin">*</span>100; <span class="org-comment-delimiter">% </span><span class="org-comment">Wanted crossover frequency [rad/s]</span>
a = 2; <span class="org-comment-delimiter">% </span><span class="org-comment">Lead parameter</span>

Khac = diag(1<span class="org-builtin">./</span>diag(abs(evalfr(G_dp, 1<span class="org-constant">j</span><span class="org-builtin">*</span>wc)))) <span class="org-builtin">*</span> <span class="org-comment-delimiter">.</span><span class="org-comment">.. % Diagonal controller</span>
        wc<span class="org-builtin">/</span>s <span class="org-builtin">*</span> <span class="org-comment-delimiter">.</span><span class="org-comment">.. % Integrator</span>
        1<span class="org-builtin">/</span>(sqrt(a))<span class="org-builtin">*</span>(1 <span class="org-builtin">+</span> s<span class="org-builtin">/</span>(wc<span class="org-builtin">/</span>sqrt(a)))<span class="org-builtin">/</span>(1 <span class="org-builtin">+</span> s<span class="org-builtin">/</span>(wc<span class="org-builtin">*</span>sqrt(a))) <span class="org-builtin">*</span> <span class="org-comment-delimiter">.</span><span class="org-comment">.. % Lead</span>
        1<span class="org-builtin">/</span>(s<span class="org-builtin">^</span>2<span class="org-builtin">/</span>(4<span class="org-builtin">*</span>wc)<span class="org-builtin">^</span>2 <span class="org-builtin">+</span> 2<span class="org-builtin">*</span>s<span class="org-builtin">/</span>(4<span class="org-builtin">*</span>wc) <span class="org-builtin">+</span> 1); <span class="org-comment-delimiter">% </span><span class="org-comment">Low pass filter</span>
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Save the HAC controller</span>
save(<span class="org-string">'mat/Khac_iff.mat'</span>, <span class="org-string">'Khac'</span>);
</pre>
</div>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Loop Gain</span>
L_hac_lac = G_dp <span class="org-builtin">*</span> Khac;
</pre>
</div>


<div id="orgeac5939" class="figure">
<p><img src="figs/hac_iff_loop_gain_bode_plot.png" alt="hac_iff_loop_gain_bode_plot.png" />
</p>
<p><span class="figure-number">Figure 16: </span>Bode Plot of the Loop gain for the High Authority Controller</p>
</div>

<p>
As shown in the Root Locus plot in Figure <a href="#org48f5645">17</a>, the closed loop system should be stable.
</p>


<div id="org48f5645" class="figure">
<p><img src="figs/loci_hac_iff_fast_jack.png" alt="loci_hac_iff_fast_jack.png" />
</p>
<p><span class="figure-number">Figure 17: </span>Root Locus for the High Authority Controller</p>
</div>
</div>
</div>

<div id="outline-container-org3cc0706" class="outline-3">
<h3 id="org3cc0706"><span class="section-number-3">6.3.</span> Performances</h3>
<div class="outline-text-3" id="text-6-3">
<p>
In order to estimate the performances of the HAC-IFF control strategy, the transfer function from motion errors of the stepper motors to the motion error of the crystal is identified both in open loop and with the HAC-IFF strategy.
</p>

<p>
It is first verified that the closed-loop system is stable:
</p>

<div class="org-src-container">
<pre class="src src-matlab">isstable(T_hl)
</pre>
</div>

<pre class="example">
1
</pre>


<p>
And both transmissibilities are compared in Figure <a href="#org9714ce1">18</a>.
</p>


<div id="org9714ce1" class="figure">
<p><img src="figs/stepper_transmissibility_comp_ol_hac_iff.png" alt="stepper_transmissibility_comp_ol_hac_iff.png" />
</p>
<p><span class="figure-number">Figure 18: </span>Comparison of the transmissibility of errors from vibrations of the stepper motor between the open-loop case and the hac-iff case.</p>
</div>

<div class="important" id="org9958f45">
<p>
The HAC-IFF control strategy can effectively reduce the transmissibility of the motion errors of the stepper motors.
This reduction is effective inside the bandwidth of the controller.
</p>

</div>
</div>
</div>

<div id="outline-container-org918f082" class="outline-3">
<h3 id="org918f082"><span class="section-number-3">6.4.</span> Close Loop noise budget</h3>
<div class="outline-text-3" id="text-6-4">
<p>
Let&rsquo;s compute the amplitude spectral density of the jack motion errors due to the sensor noise, the actuator noise and disturbances.
</p>

<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% Computation of ASD of contribution of inputs to the closed-loop motion</span>
<span class="org-comment-delimiter">% </span><span class="org-comment">Error due to disturbances</span>
asd_d = abs(squeeze(freqresp(Wd<span class="org-builtin">*</span>(1<span class="org-builtin">/</span>(1 <span class="org-builtin">+</span> G_dp(1,1)<span class="org-builtin">*</span>Khac(1,1))), f, <span class="org-string">'Hz'</span>)));
<span class="org-comment-delimiter">% </span><span class="org-comment">Error due to actuator noise</span>
asd_u = abs(squeeze(freqresp(Wu<span class="org-builtin">*</span>(G_dp(1,1)<span class="org-builtin">/</span>(1 <span class="org-builtin">+</span> G_dp(1,1)<span class="org-builtin">*</span>Khac(1,1))), f, <span class="org-string">'Hz'</span>)));
<span class="org-comment-delimiter">% </span><span class="org-comment">Error due to sensor noise</span>
asd_n = abs(squeeze(freqresp(Wn<span class="org-builtin">*</span>(G_dp(1,1)<span class="org-builtin">*</span>Khac(1,1)<span class="org-builtin">/</span>(1 <span class="org-builtin">+</span> G_dp(1,1)<span class="org-builtin">*</span>Khac(1,1))), f, <span class="org-string">'Hz'</span>)));
</pre>
</div>

<p>
The closed-loop ASD is then:
</p>
<div class="org-src-container">
<pre class="src src-matlab"><span class="org-matlab-cellbreak">%% ASD of the closed-loop motion</span>
asd_cl = sqrt(asd_d<span class="org-builtin">.^</span>2 <span class="org-builtin">+</span> asd_u<span class="org-builtin">.^</span>2 <span class="org-builtin">+</span> asd_n<span class="org-builtin">.^</span>2);
</pre>
</div>

<p>
The obtained ASD are shown in Figure <a href="#org0490d21">19</a>.
</p>


<div id="org0490d21" class="figure">
<p><img src="figs/close_loop_asd_noise_budget_hac_iff.png" alt="close_loop_asd_noise_budget_hac_iff.png" />
</p>
<p><span class="figure-number">Figure 19: </span>Closed Loop noise budget</p>
</div>

<p>
Let&rsquo;s compare the open-loop and close-loop cases (Figure <a href="#orgcda44ba">20</a>).
</p>

<div id="orgcda44ba" class="figure">
<p><img src="figs/cps_comp_ol_cl_hac_iff.png" alt="cps_comp_ol_cl_hac_iff.png" />
</p>
<p><span class="figure-number">Figure 20: </span>Cumulative Power Spectrum of the open-loop and closed-loop motion error along one fast-jack</p>
</div>
</div>
</div>
</div>
</div>
<div id="postamble" class="status">
<p class="author">Author: Dehaeze Thomas</p>
<p class="date">Created: 2022-06-02 Thu 18:11</p>
</div>
</body>
</html>
