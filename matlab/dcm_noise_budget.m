% Matlab Init                                              :noexport:ignore:

%% dcm_noise_budget.m
% Basic uniaxial noise budgeting

%% Clear Workspace and Close figures
clear; close all; clc;

%% Intialize Laplace variable
s = zpk('s');

%% Path for functions, data and scripts
addpath('./mat/'); % Path for data

%% Simscape Model - Nano Hexapod
addpath('./STEPS/')

%% Colors for the figures
colors = colororder;

%% Frequency Vector
freqs = logspace(1, 3, 1000);

%% Frequency vector for noise budget [Hz]
f = logspace(-1, 3, 1000);

% Power Spectral Density of signals

% Interferometer noise:

Wn = 6e-11*(1 + s/2/pi/200)/(1 + s/2/pi/60); % m/sqrt(Hz)



% #+RESULTS:
% : Measurement noise: 0.79 [nm,rms]

% DAC noise (amplified by the PI voltage amplifier, and converted to newtons):

Wdac = tf(3e-8); % V/sqrt(Hz)
Wu = Wdac*22.5*10; % N/sqrt(Hz)



% #+RESULTS:
% : DAC noise: 0.95 [uV,rms]

% Disturbances:

Wd = 5e-7/(1 + s/2/pi); % m/sqrt(Hz)

%% Save ASD of noise and disturbances
save('mat/asd_noises_disturbances.mat', 'Wn', 'Wu', 'Wd');

% Open Loop disturbance and measurement noise
% The comparison of the amplitude spectral density of the measurement noise and of the jack parasitic motion is performed in Figure [[fig:open_loop_noise_budget_fast_jack]].
% It confirms that the sensor noise is low enough to measure the motion errors of the crystal.


%% Bode plot for the plant (strain gauge output)
figure;
hold on;
plot(f, abs(squeeze(freqresp(Wn, f, 'Hz'))), ...
     'DisplayName', 'n');
plot(f, abs(squeeze(freqresp(Wd, f, 'Hz'))), ...
     'DisplayName', 'd');
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
xlabel('Frequency [Hz]'); ylabel('ASD [$m/\sqrt{Hz}$]');
legend('location', 'northeast', 'FontSize', 8, 'NumColumns', 2);
xlim([f(1), f(end)]);
