% Matlab Init                                              :noexport:ignore:

%% dcm_identification.m
% Extraction of system dynamics using Simscape model

%% Clear Workspace and Close figures
clear; close all; clc;

%% Intialize Laplace variable
s = zpk('s');

%% Path for functions, data and scripts
addpath('./mat/'); % Path for data

%% Simscape Model - Nano Hexapod
addpath('./STEPS/')

%% Initialize Parameters for Simscape model
controller.type = 0; % Open Loop Control

%% Options for Linearization
options = linearizeOptions;
options.SampleTime = 0;

%% Open Simulink Model
mdl = 'simscape_dcm_simplified';

open(mdl)

%% Colors for the figures
colors = colororder;

%% Frequency Vector
freqs = logspace(1, 3, 1000);

% Parameters

% Actuator Jacobian for Ring
J_a_r = [1,  0.14, -0.0675
         1,  0.14,  0.1525
         1, -0.14,  0.0425];

% Actuator Jacobian for Hall
J_a_h = [1,  0.14, -0.1525
         1,  0.14,  0.0675
         1, -0.14, -0.0425];

% Sensor Jacobian for Metrology Frame - Ring
J_s_f_r = [1,  0.102,  0.0425
           1, -0.088,  0.170
           1, -0.088, -0.085];

% Sensor Jacobian for Metrology Frame - Hall
J_s_f_h = [1,  0.102, -0.0425
           1, -0.088,  0.085
           1, -0.088, -0.170];

% Sensor Jacobian - 1st Crystal Ring
J_s_1_r = [-1, -0.036, -0.015
           -1,  0,      0.015
           -1,  0.036, -0.015];

% Sensor Jacobian - 1st Crystal Hall
J_s_1_h = [-1, -0.036,  0.015
           -1,  0,     -0.015
           -1,  0.036,  0.015];

% Sensor Jacobian - 2nd Crystal Ring
J_s_2_r = [1,  0.07,  0.015
           1,  0,    -0.015
           1, -0.07,  0.015];

% Sensor Jacobian - 2nd Crystal Hall
J_s_2_h = [1,  0.07, -0.015
           1,  0,     0.015
           1, -0.07, -0.015];

v_to_f = 1e-3; % N/V

xtal = 0; % 0:ring, 1:hall

k_pz = 13e6;
c_pz = 1e2;



% #+name: fig:schematic_system_inputs_outputs
% #+caption: Dynamical system with inputs and outputs
% #+RESULTS:
% [[file:figs/schematic_system_inputs_outputs.png]]

% The system is identified from the Simscape model.


%% Input/Output definition
clear io; io_i = 1;

%% Inputs
% Control Input {3x1} [mV]
io(io_i) = linio([mdl, '/regulator_hac'], 1, 'openinput');  io_i = io_i + 1;

%% Outputs
% Interferometers {3x1} [nm]
io(io_i) = linio([mdl, '/plant'], 1, 'openoutput'); io_i = io_i + 1;

%% Extraction of the dynamics
xtal = 0; % 0:ring, 1:hall
Gr = linearize(mdl, io);
xtal = 1; % 0:ring, 1:hall
Gh = linearize(mdl, io);

%% Input and Output names
Gr.InputName  = {'u_ur',  'u_uh',  'u_d'};
Gr.OutputName = {'int_2r_u', 'int_2r_c', 'int_2r_d'};
Gh.InputName  = {'u_ur',  'u_uh',  'u_d'};
Gh.OutputName = {'int_2h_u', 'int_2h_c', 'int_2h_d'};

save('plant_simple.mat', 'G')



% #+name: fig:schematic_jacobian_frame_fastjack
% #+caption: Use of Jacobian matrices to obtain the system in the frame of the fastjacks
% #+RESULTS:
% [[file:figs/schematic_jacobian_frame_fastjack.png]]



%% Compute the system in the frame of the fastjacks
Gr_pz = J_a_r*inv(J_s_2_r)*Gr;
Gh_pz = J_a_h*inv(J_s_2_h)*Gh;



% #+name: tab:dc_gain_plan_fj
% #+caption: DC gain of the plant in the frame of the fast jacks $\bm{G}_{\text{fj}}$
% #+attr_latex: :environment tabularx :width 0.5\linewidth :align ccc
% #+attr_latex: :center t :booktabs t
% #+RESULTS:
% | 9.9998733e-01  -1.3395804e-06   1.4005091e-05  |
% | -1.2694169e-06   1.0000198e+00  -1.8548538e-05 |
% | 2.0759261e-05  -2.3501689e-05   1.0000027e+00  |


%% Bode plot for the plant
freqs = logspace(0,3,1000);
figure;
tiledlayout(3, 1, 'TileSpacing', 'Compact', 'Padding', 'None');

ax1 = nexttile([2,1]);
hold on;
plot(freqs, abs(squeeze(freqresp(Gh_pz(1,1), freqs, 'Hz'))), ...
     'DisplayName', 'ur');
plot(freqs, abs(squeeze(freqresp(Gh_pz(2,2), freqs, 'Hz'))), ...
     'DisplayName', 'uh');
plot(freqs, abs(squeeze(freqresp(Gh_pz(3,3), freqs, 'Hz'))), ...
     'DisplayName', 'd');
for i = 1:2
    for j = i+1:3
        plot(freqs, abs(squeeze(freqresp(Gh_pz(i,j), freqs, 'Hz'))), 'color', [0, 0, 0, 0.2], ...
             'HandleVisibility', 'off');
    end
end
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
ylabel('Amplitude [m/N]'); set(gca, 'XTickLabel',[]);
legend('location', 'southeast', 'FontSize', 8, 'NumColumns', 3);
ylim([1e-2, 1e2]);

ax2 = nexttile;
hold on;
plot(freqs, 180/pi*angle(squeeze(freqresp(Gh_pz(1,1), freqs, 'Hz'))));
plot(freqs, 180/pi*angle(squeeze(freqresp(Gh_pz(2,2), freqs, 'Hz'))));
plot(freqs, 180/pi*angle(squeeze(freqresp(Gh_pz(3,3), freqs, 'Hz'))));
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'lin');
xlabel('Frequency [Hz]'); ylabel('Phase [deg]');
hold off;
yticks(-360:90:360);
ylim([-180, 180]);

linkaxes([ax1,ax2],'x');
xlim([1, 1e3]);

% Identification
% - [ ] 2024-08-01_10-13-25_plant_ring.mat
% - [ ] 2024-08-01_10-15-35_plant_ring.mat
% - [ ] 2024-08-01_10-17-51_plant_ring.mat
% - [ ] 2024-08-01_10-23-50_plant_ring_sweep.mat
% - [ ] 2024-08-01_10-25-19_plant_ring_sweep.mat
% - [ ] 2024-08-01_10-27-02_plant_ring_sweep.mat
% - [ ] 2024-08-01_10-28-27_plant_ring_sweep.mat
% - [ ] 2024-08-01_10-38-29_plant_ring_long.mat


%% Load Data
data_dir = "/home/thomas/mnt/data_jazzy/id21/inhouse/DCM/CALIB/DYNAMICS";
data = load(sprintf("%s/2024-08-01_10-25-19_plant_ring_sweep.mat", data_dir));

%% Prepare frequency Analysis
% Sampling Time [s]
Ts = 1e-4;

% Hannning Windows
Nfft = floor(1.0/Ts);
win = hanning(Nfft);
Noverlap = floor(Nfft/2);

% And we get the frequency vector
[~, f] = tfestimate(data.ur.id_exc, data.ur.y_fj_ur, win, Noverlap, Nfft, 1/Ts);

%% Identify plant
G = zeros(length(f), 3, 3);
strut_name = {'ur', 'uh', 'd'}
for i_strut = 1:3
    eL = detrend([data.(strut_name{i_strut}).y_fj_ur ; data.(strut_name{i_strut}).y_fj_uh ; data.(strut_name{i_strut}).y_fj_d]', 0);

    G(:,:,i_strut) = tfestimate(data.(strut_name{i_strut}).id_exc, eL, win, Noverlap, Nfft, 1/Ts);
end

%% Plant Dynamics
figure;
tiledlayout(3, 1, 'TileSpacing', 'compact', 'Padding', 'None');

ax1 = nexttile([2,1]);
hold on;
plot(f, abs(G(:, 1, 2)), 'color', [0,0,0,0.2], ...
     'DisplayName', 'Coupling');
for i = 1:2
    for j = i+1:3
        plot(f, abs(G(:, i, j)), 'color', [0,0,0,0.2], ...
             'HandleVisibility', 'off');
    end
end
plot(f, abs(G(:, 1, 1)), 'color', [colors(1,:)], ...
     'DisplayName', '$u_r$');
plot(f, abs(G(:, 2, 2)), 'color', [colors(2,:)], ...
     'DisplayName', '$u_h$');
plot(f, abs(G(:, 3, 3)), 'color', [colors(3,:)], ...
     'DisplayName', '$d$');
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
ylabel('Amplitude [nm/mV]'); set(gca, 'XTickLabel',[]);
% ylim([1e-8, 2e-4]);
legend('location', 'southeast', 'FontSize', 8, 'NumColumns', 2);

ax2 = nexttile;
hold on;
for i = 1:3
    plot(f, 180/pi*angle(G(:,i, i)), 'color', [colors(i,:)]);
end
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'lin');
xlabel('Frequency [Hz]'); ylabel('Phase [deg]');
hold off;
yticks(-360:90:360);
ylim([-180, 180])

linkaxes([ax1,ax2],'x');
xlim([1, 1e3]);

% Compare with plant

freqs = logspace(1,3,1000);

%% Plant Dynamics
figure;
tiledlayout(3, 1, 'TileSpacing', 'compact', 'Padding', 'None');

ax1 = nexttile([2,1]);
hold on;
plot(f, abs(G(:,1,2)), 'color', [0,0,0,0.2], 'DisplayName', 'Coupling');
plot(f, abs(G(:,2,1)), 'color', [0,0,0,0.2], 'HandleVisibility', 'off');
plot(f, abs(G(:,1,3)), 'color', [0,0,0,0.2], 'HandleVisibility', 'off');
plot(f, abs(G(:,3,1)), 'color', [0,0,0,0.2], 'HandleVisibility', 'off');
plot(f, abs(G(:,2,3)), 'color', [0,0,0,0.2], 'HandleVisibility', 'off');
plot(f, abs(G(:,3,2)), 'color', [0,0,0,0.2], 'HandleVisibility', 'off');
plot(freqs, abs(squeeze(freqresp(Gr_pz(1,2), freqs, 'Hz'))), '--', 'color', [0,0,0,0.2], 'HandleVisibility', 'off')
plot(freqs, abs(squeeze(freqresp(Gr_pz(2,1), freqs, 'Hz'))), '--', 'color', [0,0,0,0.2], 'HandleVisibility', 'off')
plot(freqs, abs(squeeze(freqresp(Gr_pz(1,3), freqs, 'Hz'))), '--', 'color', [0,0,0,0.2], 'HandleVisibility', 'off')
plot(freqs, abs(squeeze(freqresp(Gr_pz(3,1), freqs, 'Hz'))), '--', 'color', [0,0,0,0.2], 'HandleVisibility', 'off')
plot(freqs, abs(squeeze(freqresp(Gr_pz(2,3), freqs, 'Hz'))), '--', 'color', [0,0,0,0.2], 'HandleVisibility', 'off')
plot(freqs, abs(squeeze(freqresp(Gr_pz(3,2), freqs, 'Hz'))), '--', 'color', [0,0,0,0.2], 'HandleVisibility', 'off')
plot(f, abs(G(:, 1, 1)), 'color', [colors(1,:)], 'DisplayName', '$u_r$');
plot(f, abs(G(:, 2, 2)), 'color', [colors(2,:)], 'DisplayName', '$u_h$');
plot(f, abs(G(:, 3, 3)), 'color', [colors(3,:)], 'DisplayName', '$d$');
plot(freqs, abs(squeeze(freqresp(Gr_pz(1,1), freqs, 'Hz'))), '--', 'color', [colors(1,:)], 'DisplayName', 'Model')
plot(freqs, abs(squeeze(freqresp(Gr_pz(2,2), freqs, 'Hz'))), '--', 'color', [colors(2,:)], 'HandleVisibility', 'off')
plot(freqs, abs(squeeze(freqresp(Gr_pz(3,3), freqs, 'Hz'))), '--', 'color', [colors(3,:)], 'HandleVisibility', 'off')
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
ylabel('Amplitude [nm/mV]'); set(gca, 'XTickLabel',[]);
legend('location', 'southeast', 'FontSize', 8, 'NumColumns', 2);

ax2 = nexttile;
hold on;
plot(f, 180/pi*angle(G(:,1,1)), 'color', [colors(1,:)]);
plot(f, 180/pi*angle(G(:,2,2)), 'color', [colors(2,:)]);
plot(f, 180/pi*angle(G(:,3,3)), 'color', [colors(3,:)]);
plot(freqs, 180/pi*angle(squeeze(freqresp(Gr_pz(1,1), freqs, 'Hz'))), '--', 'color', [colors(1,:)]);
plot(freqs, 180/pi*angle(squeeze(freqresp(Gr_pz(2,2), freqs, 'Hz'))), '--', 'color', [colors(2,:)]);
plot(freqs, 180/pi*angle(squeeze(freqresp(Gr_pz(3,3), freqs, 'Hz'))), '--', 'color', [colors(3,:)]);
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'lin');
xlabel('Frequency [Hz]'); ylabel('Phase [deg]');
hold off;
yticks(-360:90:360);
ylim([-180, 180])

linkaxes([ax1,ax2],'x');
xlim([1, 1e3]);



% #+name: fig:schematic_jacobian_frame_crystal
% #+caption: Use of Jacobian matrices to obtain the system in the frame of the crystal
% #+RESULTS:
% [[file:figs/schematic_jacobian_frame_crystal.png]]


Gr_mr = inv(J_s_2_r)*Gr*inv(J_a_r');



% #+RESULTS:
% | 4.4964313e-01  -8.9289209e-01  -1.7562707e+00  |
% | -8.9290334e-01   1.9132749e+01   9.8161690e-04 |
% | -1.7562439e+00   7.9270202e-04   4.1322516e+01 |


%% Bode plot for the plant
figure;
tiledlayout(3, 1, 'TileSpacing', 'Compact', 'Padding', 'None');

ax1 = nexttile([2,1]);
hold on;
plot(freqs, abs(squeeze(freqresp(Gr_mr(1,1), freqs, 'Hz'))), ...
     'DisplayName', '$d_z$');
plot(freqs, abs(squeeze(freqresp(Gr_mr(2,2), freqs, 'Hz'))), ...
     'DisplayName', '$r_y$');
plot(freqs, abs(squeeze(freqresp(Gr_mr(3,3), freqs, 'Hz'))), ...
     'DisplayName', '$r_x$');
for i = 1:2
    for j = i+1:3
        plot(freqs, abs(squeeze(freqresp(Gr_mr(i,j), freqs, 'Hz'))), 'color', [0, 0, 0, 0.2], ...
             'HandleVisibility', 'off');
    end
end
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
ylabel('Amplitude [m/N]'); set(gca, 'XTickLabel',[]);
legend('location', 'southwest', 'FontSize', 8, 'NumColumns', 2);

ax2 = nexttile;
hold on;
plot(freqs, 180/pi*angle(squeeze(freqresp(Gr_mr(1,1), freqs, 'Hz'))));
plot(freqs, 180/pi*angle(squeeze(freqresp(Gr_mr(2,2), freqs, 'Hz'))));
plot(freqs, 180/pi*angle(squeeze(freqresp(Gr_mr(3,3), freqs, 'Hz'))));
hold off;
set(gca, 'XScale', 'log'); set(gca, 'YScale', 'lin');
xlabel('Frequency [Hz]'); ylabel('Phase [deg]');
hold off;
yticks(-360:90:360);
ylim([-180, 180]);

linkaxes([ax1,ax2],'x');
xlim([freqs(1), freqs(end)]);

%% Bode plot for the plant
fig = figure;
tiledlayout(3, 3, 'TileSpacing', 'Compact', 'Padding', 'None');

for i_out = 1:3
    for i_in = 1:3
        ax = nexttile;
        plot(freqs, abs(squeeze(freqresp(G_mr(i_out, i_in), freqs, 'Hz'))));
        set(gca, 'XScale', 'log'); set(gca, 'YScale', 'log');
    end
end

linkaxes(findall(fig, 'type', 'axes'),'xy');
xlim([freqs(1), freqs(end)]);
